var equipos = ["Kru", "Leviatan"]
var direccion = ["https://gitlab.com/malautaro/modelo-imagenes/-/raw/main/Imagenes/Kru.png","https://gitlab.com/malautaro/modelo-imagenes/-/raw/main/Imagenes/Leviatan.png"]
var dirTorneos = ""
var mapas = ["Ascent", "Fracture", "Haven", "Icebox", "Lotus", "Pear", "Split"]
var mapasImg = []

// Definición de variables soporte y de dom
var s = "selector"
var eq = "equipos" 
var pos = ["izq","der"]
var posLogo = ["logo-izq", "logo-der"]
var posNombre = ["nombre-izq","nombre-der"]
var puntajes = ["win-rate-","map-score-","round-winrate-atk-","round-winrate-def-"]


// Variables a completar por forEach

var listadoSelectores = []
var listadoMapas =[]
var variablesEntrada = []
var variablesEntradaFinal = []
var m = []
var divDefMapas = []
var imgInsert = []

// creacion de listas de variables compuestas
// Listado selectores equipos

pos.forEach(function(e,index){
    listadoSelectores.push(s+"-"+eq+"-"+e)
})
// Listado selectores mapas
mapas.forEach(function(e,index){
    listadoMapas.push(s+"-mapa-"+(index+1))
})

// Listado scores

pos.forEach(function(e,index){
    puntajes.forEach(function(j,index2){
        variablesEntrada.push(j+e)
    })
})

mapas.forEach(function(e,i){
    m.push("-m"+(i+1))
})

m.forEach(function(mi,index){
        variablesEntrada.forEach(function(e,i2){
        variablesEntradaFinal.push(e+mi)
        })
        }
    )

    // Listado de divs para los mapas
for (let i = 1; i < 8; i++) {
    divDefMapas.push("mapa-datos-"+i)
}


// pos.forEach mapas.forEach y variablesEntrada.forEach probablemente puedan ser 1 solo loop 

//Creación de divs por mapas donde van los datos

divDefMapas.forEach(function(ele, index){
    tempDiv = document.createElement("div");
        tempDiv.setAttribute("id",ele);
        tempDiv.setAttribute("class","mapas-linea");
        br = document.createElement("br")
        document.getElementById("mapas").appendChild(tempDiv);
        document.getElementById("mapas").appendChild(br);
})

//Funciones 
//Función dropdown (Crea los dropdown de selección)
// Equipos
    listadoSelectores.forEach(function(elemento, index){
    var selEquipos = document.getElementById(elemento);
    var fragmentEquipos = document.createDocumentFragment();
    equipos.forEach(function(equipo, index) {
        var opt = document.createElement('option');
        opt.innerHTML = equipo;
        opt.value = equipo;
        fragmentEquipos.appendChild(opt);
    });
    selEquipos.appendChild(fragmentEquipos);

})
//Mapas

    listadoMapas.forEach(function(elemento, index){
    var selMapas = document.getElementById(elemento);
    var fragmentMapas = document.createDocumentFragment();
    mapas.forEach(function(mapa, index) {
        var opt = document.createElement('option');
        opt.innerHTML = mapa;
        opt.value = mapa;
        fragmentMapas.appendChild(opt);
    });
    selMapas.appendChild(fragmentMapas); 

})

//Funcion logo (asigna las imagenes para los logos: los logos tienen que tener el mismo nombre que el equipo y estar en la carpeta imagenes)
//Cambiar el codigo para que la dire sea directa para cuando este levantado 
// 

function logo(){
listadoSelectores.forEach(function(elemento, index){
    inter = document.getElementById(elemento).value;
    referenciaLogo = equipos.indexOf(inter);
    document.getElementById(posLogo[index]).src=direccion[referenciaLogo];
})}

// Función poner el nombre con el valor seleccionado 
function nombrar() { 
    listadoSelectores.forEach(function(elemento, index){
        nombre = document.getElementById(elemento).value;        
        document.getElementById(posNombre[index]).innerHTML=nombre;
})}

// Función para el título 

function titulo() {
    document.getElementById("titulo").innerHTML = document.getElementById("tname").value;
}

//Funcion para los valores de cada mapa

function datosif() {
    var check = document.getElementById("win-rate-izq-m1-mostrar")
    console.log(check)
    if (check == null) {
        variablesEntradaFinal.forEach(function(ele, index){
            tempDiv = document.createElement("div");
            tempDiv.setAttribute("id",ele+"-mostrar");
            tempDiv.setAttribute("class","flex-item");
            mapNumber = ele.slice(-1)
            document.getElementById("mapa-datos-"+mapNumber).appendChild(tempDiv)
            valor = document.getElementById(ele).value;
            document.getElementById(ele+"-mostrar").innerHTML=valor});
    }
    else {
        variablesEntradaFinal.forEach(function(ele, index){
            valor = document.getElementById(ele).value;
            document.getElementById(ele+"-mostrar").innerHTML=valor
        })
    }
}

//Funcion para sacar las ID para insertar las imagenes de los mapas

function imgExtract() {
    for (let i = 4; i < 57; i = i+8) {
        imgInsert.push(variablesEntradaFinal[i])
    }
}

// Función para insertar las imagenes de los mapas

/*
test = document.createElement("img")
test.setAttribute("src", "https://cdn.thespike.gg/Teams%2Fbonk_1598308784074.png")
ain =  document.getElementById("win-rate-der-m1-mostrar")
padre = document.getElementById("win-rate-der-m1-mostrar").parentNode */ 

// Función para el bóton que tiene todas las funciones

function todas(){
logo()
nombrar()
titulo()
datosif()
imgExtract()

padre.insertBefore(test,ain)
}

